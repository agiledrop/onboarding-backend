<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['jwt.verify']], function () {
    // Locations endpoints
    Route::get('locations', function () {
        return new \App\Http\Resources\LocationsCollection(\App\Location::paginate(10));
    });
    Route::get('locations/all', function () {
        return new \App\Http\Resources\LocationsCollection(\App\Location::all());
    });
    Route::get('locations/{id}', 'LocationsApiController@getLocation');
    Route::post('locations', 'LocationsApiController@createLocation');
    Route::put('locations/{id}', 'LocationsApiController@updateLocation');
    Route::delete('locations/{id}', 'LocationsApiController@deleteLocation');

    // Companies endpoints
    Route::get('companies', function () {
        return new \App\Http\Resources\CompanyCollection(\App\Company::paginate(10));
    });
    Route::get('companies/all', function() {
        return new \App\Http\Resources\CompanyCollection(\App\Company::all());
    });
    Route::get('companies/{id}', 'CompaniesApiController@getCompany');
    Route::post('companies', 'CompaniesApiController@createCompany');
    Route::put('companies/{id}', 'CompaniesApiController@updateCompany');
    Route::delete('companies/{id}', 'CompaniesApiController@deleteCompany');
    // Events endpoints
    Route::get('/events', function () {
        return new \App\Http\Resources\EventCollection(\App\Event::paginate(10));
    });
    Route::get('events/all', function () {
        return new \App\Http\Resources\EventCollection(\App\Event::all());
    });
    Route::get('events/{id}', 'EventsApiController@getEvent');
    Route::post('events', 'EventsApiController@createEvent');
    Route::put('events/{id}', 'EventsApiController@updateEvent');
    Route::delete('events/{id}', 'EventsApiController@deleteEvent');

    Route::get('users', function () {
        return new \App\Http\Resources\UsersCollection(\App\User::all());
    });
    Route::put('userStatus', 'UsersApiController@updateUserStatuses');
});

// User/Login endpoints
Route::post('login', 'UsersApiController@login');
