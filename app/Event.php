<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $primaryKey = 'id';

    public function location() {
        return $this->belongsTo('App\Location');
    }

    public function company() {
        return $this->belongsTo('App\Company');
    }
}
