<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';

    public function events() {
        return $this->hasMany('App\Event');
    }

    public function location() {
        return $this->belongsTo('App\Location');
    }
}
