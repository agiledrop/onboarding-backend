<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

class LocationsApiController extends Controller
{
    public function createLocation(Request $request) {
        $location = new Location();
        $location->name = $request->name;
        $location->country = $request->country;
        $location->save();

        return response()->json([
            "data" => new \App\Http\Resources\Location($location),
        ], 201);
    }

    public function getLocation($id) {
        if (Location::where('id', $id)->exists()) {
            $location = Location::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($location, 200);
        } else {
            return response()->json([
                "message" => "Location doesn't exist"
            ], 404);
        }
    }

    public function updateLocation(Request $request, $id) {
        if (Location::where('id', $id)->exists()) {
            $location = Location::find($id);
            $location->name = is_null($request->name) ? $location->name : $request->name;
            $location->country = is_null($request->country) ? $location->country : $request->country;
            $location->save();

            return response()->json([
                "data" => new \App\Http\Resources\Location($location),
            ], 200);
        } else {
            return response()->json([
                "message" => "Location doesn't exist"
            ], 404);
        }
    }

    public function deleteLocation(Request $request, $id) {
        if (Location::where('id', $id)->exists()) {
            $location = Location::find($id);
            $location->delete();

            return response()->json([
                "data" => $location->id,
            ], 202);
        } else {
            return response()->json([
                "message" => "Location doesn't exist"
            ], 404);
        }
    }
}
