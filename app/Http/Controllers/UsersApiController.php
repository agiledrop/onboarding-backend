<?php

namespace App\Http\Controllers;

use App\Http\Resources\UsersCollection;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Auth;

class UsersApiController extends Controller
{
    public function login(Request $request) {
        $username = $request->username;
        $password = $request->password;

        if ($token = Auth::attempt(['username' => $username, 'password' => $password])) {
            $user = new UserResource(User::where('username', $username)->first(), $token);
            return response($user, 200);
        } else {
            return response()->json([
                "message" => "Invalid credentials"
            ], 400);
        }
    }

    public function updateUserStatuses(Request $request) {
        $userItems = $request->data;

        foreach ($userItems as $item) {
            if (User::where('id', $item["id"])->exists()) {
                $user = User::find($item["id"]);
                $user->admin = $item["admin"];
                $user->save();
            }
        }
        return response()->json([
            "users" => new UsersCollection(User::all())
        ], 200);
    }

    public function getUsers() {}

    public function getUser() {}

    protected function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => \auth()->factory()->getTTL() * 60
        ]);
    }
}
