<?php

namespace App\Http\Controllers;

use App\Company;
use App\Location;
use App\Http\Resources\Company as CompanyResource;
use Illuminate\Http\Request;

class CompaniesApiController extends Controller
{
    public function createCompany(Request $request) {
        if (!Location::where('id', $request->location_id)->exists()) {
            return response()->json([
                "message" => "The provided location doesn't exist"
            ], 400);
        }
        $company = new Company();
        $company->name = $request->name;
        $company->location_id = $request->location_id;
        $company->save();

        return response()->json([
            "data" => new CompanyResource($company),
        ], 201);
    }

    public function getCompany($id) {
        if (Company::where('id', $id)->exists()) {
            $company = new CompanyResource(Company::find($id));
            return response($company, 200);
        } else {
            return response()->json([
                "message" => "Company doesn't exist"
            ], 404);
        }
    }

    public function updateCompany(Request $request, $id) {
        if (Company::where('id', $id)->exists()) {
            $company = Company::find($id);
            $company->name = is_null($request->name) ? $company->name : $request->name;
            if (!Location::where('id', $request->location_id)->exists()) {
                return response()->json([
                    "message" => "The location provided doesn't exist"
                ], 400);
            }
            $company->location_id = is_null($request->location_id) ? $company->location_id : $request->location_id;
            $company->save();

            return response()->json([
                "data" => new CompanyResource($company),
            ], 200);
        } else {
            return response()->json([
               "message" => "Company doesn't exist"
            ], 404);
        }
    }

    public function deleteCompany(Request $request, $id) {
        if (Company::where('id', $id)->exists()) {
            $company = Company::find($id);
            $company->delete();

            return response()->json([
                "data" => $company->id,
            ], 202);
        } else {
            return response()->json([
                "message" => "Company doesn't exist"
            ], 404);
        }
    }
}
