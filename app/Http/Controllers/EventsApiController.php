<?php

namespace App\Http\Controllers;

use App\Company;
use App\Event;
use App\Location;
use Illuminate\Http\Request;
use App\Http\Resources\Event as EventResource;

class EventsApiController extends Controller
{
    public function createEvent(Request $request) {
        if (!Location::where('id', $request->location_id)->exists() || !Company::where('id', $request->company_id)->exists()) {
            return response()->json([
                "message" => "The provided location or company doesn't exist"
            ], 400);
        }
        $event = new Event();
        $event->name = $request->name;
        $event->location_id = $request->location_id;
        $event->company_id = $request->company_id;
        $event->save();

        return response()->json([
            "data" => new EventResource($event),
        ], 201);
    }

    public function getEvent($id) {
        if (Event::where('id', $id)->exists()) {
            $event = new EventResource(Event::find($id));
            return response($event, 200);
        } else {
            return response()->json([
               "message" => "Event doesn't exist"
            ], 404);
        }
    }

    public function updateEvent(Request $request, $id) {
        if (!Company::where('id', $request->company_id)->exists()) {
            return response()->json([
                "message" => "The provided company doesn't exist"
            ], 400);
        }
        if (!Location::where('id', $request->location_id)->exists()) {
            return response()->json([
                "message" => "The provided location doesn't exist"
            ], 400);
        }
        if (Event::where('id', $id)->exists()) {
            $event = Event::find($id);
            $event->name = is_null($request->name) ? $event->name : $request->name;
            $event->location_id = is_null($request->location_id) ? $event->location_id : $request->location_id;
            $event->company_id = is_null($request->company_id) ? $event->company_id : $request->company_id;
            $event->save();

            return response()->json([
                "data" => new EventResource($event),
            ], 200);
        } else {
            return response()->json([
                "message" => "Event doesn't exist"
            ], 404);
        }
    }

    public function deleteEvent(Request $request, $id) {
        if (Event::where('id', $id)->exists()) {
            $event = Event::find($id);
            $event->delete();

            return response()->json([
                "data" => $event->id,
            ], 202);
        } else {
            return response()->json([
                "message" => "Event doesn't exist"
            ], 404);
        }
    }
}
