<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $primaryKey = 'id';

    public function events() {
        return $this->hasMany('App\Event');
    }

    public function companies() {
        return $this->hasMany('App\Company');
    }
}
