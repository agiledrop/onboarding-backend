<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    $location_ids = \App\Location::all()->pluck('id')->toArray();
    $company_ids = \App\Company::all()->pluck('id')->toArray();
    return [
        'name' => $faker->streetName,
        'company_id' => $faker->randomElement($company_ids),
        'location_id' => $faker->randomElement($location_ids),
    ];
});
