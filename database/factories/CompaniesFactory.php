<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    $location_ids = \App\Location::all()->pluck('id')->toArray();
    return [
        'name' => $faker->company,
        'location_id' => $faker->randomElement($location_ids),
    ];
});
