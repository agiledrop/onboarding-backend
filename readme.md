## Setup

In the root directory copy/rename the `.env.example` file to `.env`

In the root dir run `composer install`

Set these environment variables:
`DB_CONNECTION=mysql`
`DB_HOST=localhost`
`DB_PORT=33060`
`DB_DATABASE=homestead`
`DB_USERNAME=homestead`
`DB_PASSWORD=secret`
`APP_LOG_FILE=/code/storage/logs`

In the root dir run `php artisan key:generate` to generate the `APPLICATION_KEY`.

In the `homestead.yaml` file on line 10 change the path `map: /home/roki/Projects/OnboardingBackend` to your local setup

In the root dir run `php artisan jwt:secret` to generate a `JWT_SECRET`

In the root directory run `vagrant up`.

SSH to the vagrant box using `vagrant ssh`. Navigate to the /code dir using `cd code`
and run `php artisan migrate:refresh --seed` to create the DB tables and populate with fake data.

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

